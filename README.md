# turnup_entry_front
This is the front end writte in React

## Run the applcation inside the docker container

## Build the image
```bash
docker build -t app-front .
```

## Run the image 
```bash
docker run --name cutomer-front --rm -d -p 3000:3000 app-front
```


# Bulding and pushing the image

```bash
aws ecr get-login-password --region ca-central-1 --profile trnip | docker login --username AWS --password-stdin 676180064909.dkr.ecr.ca-central-1.amazonaws.com
```

```bash
docker build -t trnip_customer_front:latest ./app -f ./app/dockerfiles/Dockerfile.prod
```

```bash
docker tag trnip_customer_front:latest 676180064909.dkr.ecr.ca-central-1.amazonaws.com/trnip/trnip_customer_front:latest
```

```bash
docker push 676180064909.dkr.ecr.ca-central-1.amazonaws.com/trnip/trnip_customer_front:latest
```


