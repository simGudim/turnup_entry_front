import React from "react";
import '../styles/VideoChatPage.css'; // Import the CSS file

const VideoChatPage = () => {
    let { user } = useContext(AuthContext);
    let localStream;
    let remoteStream;
    let peerConnection;
    const servers = {
        iceServers: [
            {
                urls:[
                    "stun:stun.l.google.com:19302", 
                    "stun:stun2.l.google.com:19302"
                ]
            }
        ]
    }
    let init = async () => {
        localStream = await navigator.mediaDevices.getUserMedia({video: true, audio: false});
        document.getElementById("user-1").srcObject = localStream;
        createOffer()
    }

    let createOffer = async () => {
        peerConnection = new RTCPeerConnection(servers);
        remoteStream = new MediaStream();
        document.getElementById("user-2").srcObject = remoteStream;

        localStream.getTracks().forEach((track) => {
            peerConnection.addTrack(track, localStream)
        });

        peerConnection.ontrack = (event) => {
            event.streams[0].getTracks().forEach((track) => {
                remoteStream.addTrack()
            })
        };

        peerConnection.onicecandidate = async (event) => {
            if (event.candidate) {
                console.log("new ice candidate", event.candidate)
            }
        };

        let offer = await peerConnection.createOffer();
        await peerConnection.setLocalDescription(offer);
        console.log(offer)
    }

    let connectToSignallingServer = async () => {
        const ws = new WebSocket("ws://your-backend-address:port/ws");

        ws.onopen = () => {
            setWebsocket(ws);
        };

        ws.onmessage = async (event) => {
            const message = JSON.parse(event.data);
            if (message.type === "offer" || message.type === "answer") {
                // Handle SDP messages
                console.log("SDP received:", message);
                await rtcConnection.setRemoteDescription(new RTCSessionDescription(message));
            } else if (message.type === "candidate") {
                // Handle ICE Candidate
                console.log("ICE Candidate received:", message.candidate);
                rtcConnection.addIceCandidate(new RTCIceCandidate(message.candidate));
            }
        };
    };

    init()
    return (
        <div id="videos"> {/* Apply the CSS class */}
            <video className="video-player" id="user-1" autoPlay playsInline></video>
            <video className="video-player" id="user-2" autoPlay playsInline></video>
        </div>
    );
}

export default VideoChatPage;