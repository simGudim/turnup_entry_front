import React, { useState, useContext } from 'react';
import { useNavigate } from "react-router-dom";
import { WithContext as ReactTags } from 'react-tag-input';
import AuthContext from "../context/AuthContext";
import '../styles/UserProfilePage.css';

const UserProfile = () => {
    let { user, logoutUser } = useContext(AuthContext);
    const [error, setError] = useState('');
    const usersProfileApiUrl = process.env.REACT_APP_USER_PROFILE_API || '';
    const navigate = useNavigate();
    const [userProfile, setUserProfile] = useState({
        id: user.user_id,
        first_name: '',
        last_name: '',
        user_gender: '',
        user_age: '',
        native_languages: [],
        learning_languages: [],
        user_interests: [],
        user_bio: '',
        city_name: '',
        region_name: '',
        country_name: '',
    });

    const getMissingFields = () => {
        const requiredFields = {
            first_name: 'First Name',
            last_name: 'Last Name',
            user_gender: 'Gender',
            user_age: 'Age',
            native_languages: 'Native Languages',
            learning_languages: 'Learning Languages',
            user_interests: 'Interests',
            user_bio: 'Bio',
            city_name: 'City',
            region_name: 'Region',
            country_name: 'Country',
        };
    
        const missingFields = Object.entries(requiredFields).filter(([key, _]) => {
            return userProfile[key] === '';
        }).map(([_, value]) => value);

        if (userProfile.native_languages.length === 0) {
            missingFields.push("Native Languages");
        }

        if (userProfile.learning_languages.length === 0) {
            missingFields.push("Learning Languages");
        }

        try {
            userProfile.user_age = parseInt(userProfile.user_age);
        } catch {
            missingFields.push("Age");
        }

        return missingFields;
    };

    const handleChange = (e) => {
        const { name, value } = e.target;
        setUserProfile({ ...userProfile, [name]: value });
    };

    const handleAddition = (tag) => {
        setUserProfile({
            ...userProfile,
            user_interests: [...userProfile.user_interests, tag.text] // Store only the text of the tag
        });
    };
    
    const handleDelete = (i) => {
        const newTags = userProfile.user_interests.filter((_, index) => index !== i);
        setUserProfile({
            ...userProfile,
            user_interests: newTags
        });
    };

    const languages = [
        'English', 'Spanish', 'French', 'German', 'Chinese', 'Japanese', 'Korean', 'Russian', 'Arabic', 'Portuguese'
    ];


    const handleSubmit = async (event) => {
        event.preventDefault();
        const missingFields = getMissingFields();

        if (missingFields.length === 0) {
            console.log('All fields are filled. Proceed with submission.');
            try {
                await fetch(
                    `${usersProfileApiUrl}/user_profile_api/user`, 
                    {
                        method:'POST',
                        headers:{
                            'Content-Type':'application/json'
                        },
                        body:JSON.stringify(userProfile)
                    }
                )
                navigate("/login");
            } catch (error) {
                setError("User profile creation failed. Please try again.");
            } 
        } else {
            setError('Please fill in the following fields:', missingFields.join(', '));
            console.log(missingFields);
            console.log(error);
        }
    }

    const handleCheckboxChange = (e, fieldName) => {
        const { value, checked } = e.target;
        const updatedLanguages = checked
            ? [...userProfile[fieldName], value] // Add language
            : userProfile[fieldName].filter(lang => lang !== value); // Remove language

        setUserProfile({
            ...userProfile,
            [fieldName]: updatedLanguages
        });
    };

    return (
        <form className="register-form" onSubmit={handleSubmit}>
            <div className="form-group double">
                <div className="form-group-item">
                    <label>First Name:</label>
                    <input
                        type="text"
                        name="first_name"
                        value={userProfile.first_name}
                        onChange={handleChange}
                        required
                    />
                </div>

                <div className="form-group-item">
                    <label>Last Name:</label>
                    <input
                        type="text"
                        name="last_name"
                        value={userProfile.last_name}
                        onChange={handleChange}
                        required
                    />
                </div>
            </div>

            <div className="form-group double">
                <div className="form-group-item">
                    <label>Gender:</label>
                    <select
                        name="user_gender"
                        value={userProfile.user_gender}
                        onChange={handleChange}
                        required
                    >
                        <option value="">Select Gender</option>
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                        <option value="other">Other</option>
                    </select>
                </div>
                <div className="form-group-item">
                    <label>Age:</label>
                    <input
                        type="number"
                        name="user_age"
                        value={userProfile.user_age}
                        onChange={handleChange}
                        required
                    />
                </div>
            </div>

            <div className="form-group double">
                <div className="form-group-item">
                    <label>Native Languages:</label>
                    <div className="dropdown">
                        <button className="dropdown-toggle">Select Native Languages</button>
                        <div className="dropdown-content">
                            {languages.map(language => (
                                <div key={language}>
                                    <input
                                        type="checkbox"
                                        name="native_languages"
                                        value={language}
                                        checked={userProfile.native_languages.includes(language)}
                                        onChange={(e) => handleCheckboxChange(e, 'native_languages')}
                                    />
                                    <label>{language}</label>
                                </div>
                            ))}
                        </div>
                    </div>
                </div>

                <div className="form-group-item">
                    <label>Learning Languages:</label>
                    <div className="dropdown">
                        <button className="dropdown-toggle">Select Learning Languages</button>
                        <div className="dropdown-content">
                            {languages.map(language => (
                                <div key={language}>
                                    <input
                                        type="checkbox"
                                        name="learning_languages"
                                        value={language}
                                        checked={userProfile.learning_languages.includes(language)}
                                        onChange={(e) => handleCheckboxChange(e, 'learning_languages')}
                                    />
                                    <label>{language}</label>
                                </div>
                            ))}
                        </div>
                    </div>
                </div>
            </div>
            {/* User Interests */}
            {/* <div className="form-group">
                <label>Interests:</label>
                <ReactTags
                    tags={userProfile.user_interests.map((interest, index) => ({ id: index, text: interest }))}
                    handleDelete={handleDelete}
                    handleAddition={handleAddition}
                    inputFieldPosition="bottom"
                    autocomplete
                />
            </div> */}
            <div className="form-group double">
                <div className="form-group-item">
                    <label>City:</label>
                    <input
                        type="text"
                        name="city_name"
                        value={userProfile.city_name}
                        onChange={handleChange}
                        required
                    />
                </div>
                <div className="form-group-item">
                    <label>Region:</label>
                    <input
                        type="text"
                        name="region_name"
                        value={userProfile.region_name}
                        onChange={handleChange}
                        required
                    />
                </div>
                <div className="form-group-item">
                    <label>Country:</label>
                    <input
                        type="text"
                        name="country_name"
                        value={userProfile.country_name}
                        onChange={handleChange}
                        required
                    />
                </div>
            </div>

            <div className="form-group double">
                <div className="form-group-item">
                    <label>Your Bio:</label>
                    <textarea 
                        id="user_bio" 
                        name="user_bio" 
                        rows="5" 
                        placeholder="Write a brief bio..."
                        value={userProfile.bio}
                        onChange={handleChange}
                    />
                </div>
            </div>
            <button type="submit" className="submit-button">Register</button>
        </form>
    );
};

export default UserProfile;
