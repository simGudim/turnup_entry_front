import React, { useContext,useEffect, useState  } from "react";
import AuthContext from "../context/AuthContext";
import '../styles/HomePage.css'; 

const HomePage = () => {
    let { user } = useContext(AuthContext);
    const [messages, setMessages] = useState([]); // Store incoming messages
    const [connection, setConnection] = useState(null); // WebSocket connection
    const [input, setInput] = useState(""); // Input for sending messages

    useEffect(() => {
        const websocket = new WebSocket(
            `${process.env.REACT_APP_FRIEND_CHAT_API}/friends_chat_api/chat_ws_v2/00c64a7f-32f5-4919-b74c-d83ac805f215/${user.user_id}`);

        websocket.onopen = () => {
            setConnection(websocket);
        };

        websocket.onmessage = (event) => {
            setMessages((prevMessages) => [...prevMessages, event.data]); // Append new message
        };

        websocket.onclose = () => {
            setConnection(null);
        };

        websocket.onerror = (error) => {
            console.error("WebSocket error:", error);
        };

        // Cleanup function to close the connection on component unmount
        return () => {
            if (websocket) {
                websocket.close();
            }
        };
    }, []); 

    const sendMessage = () => {
        if (connection && input.trim() !== "") {
            connection.send(input); // Send the input message
            setInput(""); // Clear the input field
        }
    };

    return (
        <div>
            <h1>WebSocket Chat</h1>
            <div id="messages">
                {messages.map((message, index) => (
                    <div key={index}>{message}</div>
                ))}
            </div>
            <input
                type="text"
                value={input}
                onChange={(e) => setInput(e.target.value)}
                placeholder="Type a message..."
            />
            <button onClick={sendMessage} disabled={!connection}>
                Send
            </button>
        </div>
    );
}

export default HomePage;