import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import '../styles/RegistrationPage.css';

const RegisterPage = () => {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [email, setEmail] = useState("");
    const [error, setError] = useState("");
    const [loading, setLoading] = useState(false);
    const navigate = useNavigate();
    const usersApiUrl = process.env.REACT_APP_USERS_API || '';

    const handleRegister = async (event) => {
        event.preventDefault();
        setLoading(true);

        // Basic validation
        if (!username || !password || !email) {
            setError("All fields are required.");
            setLoading(false);
            return;
        }

        try {
            await fetch(
                `${usersApiUrl}/users_api/user`, 
                {
                    method:'POST',
                    headers:{
                        'Content-Type':'application/json'
                    },
                    body:JSON.stringify({
                        'username': username,
                        'password': password,
                        'email' : email
                    })
                }
            )
            navigate("/login");
        } catch (error) {
            setError("Registration failed. Please try again.");
        } finally {
            setLoading(false);
        }

    }
    return (
        <div>
            <h1>Register</h1>
            <form onSubmit={handleRegister}>
                <div>
                    <label htmlFor="username">Username:</label>
                    <input
                        type="text"
                        id="username"
                        value={username}
                        onChange={(e) => setUsername(e.target.value)}
                        placeholder="Enter Username"
                        required
                    />
                </div>
                <div>
                    <label htmlFor="password">Password:</label>
                    <input
                        type="password"
                        id="password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        placeholder="Enter Your Password"
                        required
                    />
                </div>
                <div>
                    <label htmlFor="email">Email:</label>
                    <input
                        type="email"
                        id="email"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        placeholder="Enter Email"
                        required
                    />
                </div>
                <div>
                    <button type="submit" disabled={loading}>
                        {loading ? "Registering..." : "Register"}
                    </button>
                </div>
                {error && <p style={{ color: "red" }}>{error}</p>}
            </form>
        </div>
    );
};

export default RegisterPage;


