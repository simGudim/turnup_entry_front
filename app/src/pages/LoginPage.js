import React, { useContext } from "react";
import { Link } from "react-router-dom";
import AuthContext from "../context/AuthContext";
import '../styles/LoginPage.css'; // Import the CSS file

const LoginPage = () => {
    let { loginUser } = useContext(AuthContext);
    return (
        <div className="login-container"> {/* Apply the CSS class */}
            <form onSubmit={loginUser}>
                <input
                    type="text"
                    name="username"
                    placeholder="Enter Username"
                    required
                />
                <input
                    type="password"
                    name="password"
                    placeholder="Enter Password"
                    required
                />
                <input
                    type="submit"
                    value="Submit"
                />
            </form>
            <p>
                Don’t have an account? <Link to="/register">Register here</Link>
            </p>
        </div>
    );
}

export default LoginPage;
