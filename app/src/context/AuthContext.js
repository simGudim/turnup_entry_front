import { createContext, useState, useEffect } from "react";
import {jwtDecode} from "jwt-decode";
import { useNavigate } from "react-router-dom";

const AuthContext = createContext()
export default AuthContext;

export const AuthProvider = ({children}) => {
    let [accessToken, setAccessToken] = useState(localStorage.getItem("accessToken"));
    let [refreshToken, setRefreshToken] = useState(localStorage.getItem("refreshToken"));
    let [user, setUser] = useState(JSON.parse(localStorage.getItem("userData")));
    let [loading, setLoading] = useState(true);
    const navigate = useNavigate();
    const usersApiUrl = process.env.REACT_APP_USERS_API || '';
    const authApiUrl = process.env.REACT_APP_AUTH_API || '';

    let loginUser = async (e) => {
        e.preventDefault()
        let response = await fetch(
            `${usersApiUrl}/users_api/user/validate`, 
            {
                method:'POST',
                headers:{
                    'Content-Type':'application/json'
                },
                body:JSON.stringify({
                    'username':e.target.username.value,
                    'password':e.target.password.value
                })
            }
        )

        if (response.status === 200) {
            let data = await response.json();
            setAccessToken(data.access_token)
            setRefreshToken(data.refresh_token)
            localStorage.setItem("accessToken", data.access_token)
            localStorage.setItem("refreshToken", data.refresh_token)

            let decoded_access_token = jwtDecode(data.access_token);
            let user_id = decoded_access_token.user_id;
            let user_info_response = await fetch(
                `${usersApiUrl}/users_api/user/` + user_id + '?query_type=user_id', 
                {
                    method:'GET',
                    headers:{
                        'Content-Type':'application/json'
                    }
                }
            )

            if (user_info_response.status === 200) {
                let user_data = await user_info_response.json();
                setUser(user_data)
                localStorage.setItem("userData", JSON.stringify(user_data))
                navigate("/")
            } else {
                alert("could not fetch user data")
            }
        } else if (response.status === 403) {
            alert("Not Authorized")
        }
    }

    let logoutUser = () => {
        setAccessToken(null)
        setRefreshToken(null)
        setUser(null)
        localStorage.removeItem("accessToken")
        localStorage.removeItem("refreshToken")
        localStorage.removeItem("userData")

        navigate("/login")
    }

    let updateToken = async () => {
        let response = await fetch(
            `${authApiUrl}/auth_api/refresh_tokens`, 
            {
                method:'POST',
                headers:{
                    'Content-Type':'application/json'
                },
                body:JSON.stringify({
                    'refresh_token':refreshToken
                })
            }
        )
        if (response.status === 200) {
            let tokenData = await response.json()
            setAccessToken(tokenData.access_token)
            setRefreshToken(tokenData.refresh_token)
            localStorage.setItem("accessToken", tokenData.access_token)
            localStorage.setItem("refreshToken", tokenData.refresh_token)
        } else {
            logoutUser()
        }

        if (loading) { 
            setLoading(false)
        }
    }

    let contextData = {
        user:user,
        loginUser:loginUser,
        logoutUser:logoutUser,
        updateToken:updateToken
    }

    useEffect(() => {
        if (loading) { 
            updateToken()
        }
        let fourMinutes = 1000 * 60 * 4
        let interval = setInterval(() => {
            if (refreshToken && accessToken) {
                updateToken()
            }
        }, fourMinutes)
        return () => clearInterval(interval)
    }, [accessToken, refreshToken, loading])

    return (
        <AuthContext.Provider value={contextData}>
            {loading ? null : children }
        </AuthContext.Provider>
    )
}