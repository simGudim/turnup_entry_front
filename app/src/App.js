import './App.css';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import HomePage from './pages/HomePage';
import LoginPage from'./pages/LoginPage';
import RegisterPage from './pages/RegisterPage';
import UserProfile from './pages/UserProfilePage';
import VideoChatPage from './pages/VideoChatPage';
import Header from './components/Header';
import PrivateRoute from './utils/PrivateRoute';
import { AuthProvider } from './context/AuthContext';

function App() {
  return (
    <div className="App">
      <Router>
        <AuthProvider>
          <Header />
          <Routes>
            <Route exact path='/' element={<PrivateRoute/>}>
              <Route exact path='/' element={<HomePage/>}/>
            </Route>
            <Route exact path='/user_profile' element={<PrivateRoute/>}>
              <Route exact path='/user_profile' element={<UserProfile/>}/>
            </Route>
            <Route exact path='/video_chat_room' element={<PrivateRoute/>}>
              <Route exact path='/video_chat_room' element={<VideoChatPage/>}/>
            </Route>
            <Route element={<LoginPage/>} path="login"/>
            <Route element={<RegisterPage/>} path="register"/>
          </Routes>
        </AuthProvider>
      </Router>
    </div>
  );
}

export default App;
