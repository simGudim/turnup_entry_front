import React, { useContext } from "react";
import { Link } from "react-router-dom";
import AuthContext from "../context/AuthContext";
import './styles/Navbar.css'

function Navbar () {
    let { user, logoutUser } = useContext(AuthContext);
    return (
        <section className="navbar">
            <Link to="/" className="navbar-item">Home</Link>
            {user ? (
                <div className="navbar-item-private">
                    <Link onClick={logoutUser} to="/login" className="navbar-item">Logout</Link>
                    {/* <Link onClick={logoutUser} to="/user_profile_edit" className="navbar-item">Profile</Link>  */}
                </div>
            ) : (
                <Link to="/login" className="navbar-item">Login</Link>
            )}
        </section>
  )

}

export default Navbar;