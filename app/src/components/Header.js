import React, { useContext } from "react";
import { Link } from "react-router-dom";
import AuthContext from "../context/AuthContext";
import './styles/Header.css'; // Import the CSS file
import  Navbar  from './Navbar'

const Header = () => {
    let { user } = useContext(AuthContext);
    return (
        <section className="header">
          <section className="header-top">
            <section className="header-top__logo">
              <a href="/" className="header-logo">LOGO</a>
            </section>
            <section className="header-top__navbar">
              <section className="header-top__navigation">
                <Navbar />
              </section>
              <hr className="header-top__seperator" />
            </section>
          </section>
          <section className="header-bottom">
            <section className="header-bottom__username">
              {user && <p className="header-bottom__username_display">{user.username}</p>}
            </section>
          </section>
        </section>
      )
}

export default Header;
